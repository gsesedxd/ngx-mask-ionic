/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/**
 * @record
 */
export function IConfig() { }
if (false) {
    /** @type {?} */
    IConfig.prototype.sufix;
    /** @type {?} */
    IConfig.prototype.prefix;
    /** @type {?} */
    IConfig.prototype.clearIfNotMatch;
    /** @type {?} */
    IConfig.prototype.showTemplate;
    /** @type {?} */
    IConfig.prototype.showMaskTyped;
    /** @type {?} */
    IConfig.prototype.dropSpecialCharacters;
    /** @type {?} */
    IConfig.prototype.specialCharacters;
    /** @type {?} */
    IConfig.prototype.patterns;
}
/** @type {?} */
export var config = new InjectionToken('config');
/** @type {?} */
export var NEW_CONFIG = new InjectionToken('NEW_CONFIG');
/** @type {?} */
export var INITIAL_CONFIG = new InjectionToken('INITIAL_CONFIG');
/** @type {?} */
export var initialConfig = {
    sufix: '',
    prefix: '',
    clearIfNotMatch: false,
    showTemplate: false,
    showMaskTyped: false,
    dropSpecialCharacters: true,
    specialCharacters: [
        '/',
        '(',
        ')',
        '.',
        ':',
        '-',
        ' ',
        '+',
        ',',
        '@',
        '[',
        ']',
        '"',
        "'"
    ],
    patterns: {
        '0': {
            pattern: new RegExp('\\d')
        },
        '9': {
            pattern: new RegExp('\\d'),
            optional: true
        },
        A: {
            pattern: new RegExp('[a-zA-Z0-9]')
        },
        S: {
            pattern: new RegExp('[a-zA-Z]')
        },
        d: {
            pattern: new RegExp('\\d')
        },
        m: {
            pattern: new RegExp('\\d')
        }
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hc2staW9uaWMvIiwic291cmNlcyI6WyJsaWIvY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBRS9DLDZCQWNDOzs7SUFiQyx3QkFBYzs7SUFDZCx5QkFBZTs7SUFDZixrQ0FBeUI7O0lBQ3pCLCtCQUFzQjs7SUFDdEIsZ0NBQXVCOztJQUN2Qix3Q0FBMEM7O0lBQzFDLG9DQUE0Qjs7SUFDNUIsMkJBS0U7OztBQUlKLE1BQU0sS0FBTyxNQUFNLEdBQTJCLElBQUksY0FBYyxDQUFDLFFBQVEsQ0FBQzs7QUFDMUUsTUFBTSxLQUFPLFVBQVUsR0FBMkIsSUFBSSxjQUFjLENBQ2xFLFlBQVksQ0FDYjs7QUFDRCxNQUFNLEtBQU8sY0FBYyxHQUE0QixJQUFJLGNBQWMsQ0FDdkUsZ0JBQWdCLENBQ2pCOztBQUVELE1BQU0sS0FBTyxhQUFhLEdBQVk7SUFDcEMsS0FBSyxFQUFFLEVBQUU7SUFDVCxNQUFNLEVBQUUsRUFBRTtJQUNWLGVBQWUsRUFBRSxLQUFLO0lBQ3RCLFlBQVksRUFBRSxLQUFLO0lBQ25CLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLHFCQUFxQixFQUFFLElBQUk7SUFDM0IsaUJBQWlCLEVBQUU7UUFDakIsR0FBRztRQUNILEdBQUc7UUFDSCxHQUFHO1FBQ0gsR0FBRztRQUNILEdBQUc7UUFDSCxHQUFHO1FBQ0gsR0FBRztRQUNILEdBQUc7UUFDSCxHQUFHO1FBQ0gsR0FBRztRQUNILEdBQUc7UUFDSCxHQUFHO1FBQ0gsR0FBRztRQUNILEdBQUc7S0FDSjtJQUNELFFBQVEsRUFBRTtRQUNSLEdBQUcsRUFBRTtZQUNILE9BQU8sRUFBRSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDM0I7UUFDRCxHQUFHLEVBQUU7WUFDSCxPQUFPLEVBQUUsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxJQUFJO1NBQ2Y7UUFDRCxDQUFDLEVBQUU7WUFDRCxPQUFPLEVBQUUsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDO1NBQ25DO1FBQ0QsQ0FBQyxFQUFFO1lBQ0QsT0FBTyxFQUFFLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQztTQUNoQztRQUNELENBQUMsRUFBRTtZQUNELE9BQU8sRUFBRSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7U0FDM0I7UUFDRCxDQUFDLEVBQUU7WUFDRCxPQUFPLEVBQUUsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDO1NBQzNCO0tBQ0Y7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElDb25maWcge1xyXG4gIHN1Zml4OiBzdHJpbmc7XHJcbiAgcHJlZml4OiBzdHJpbmc7XHJcbiAgY2xlYXJJZk5vdE1hdGNoOiBib29sZWFuO1xyXG4gIHNob3dUZW1wbGF0ZTogYm9vbGVhbjtcclxuICBzaG93TWFza1R5cGVkOiBib29sZWFuO1xyXG4gIGRyb3BTcGVjaWFsQ2hhcmFjdGVyczogYm9vbGVhbiB8IHN0cmluZ1tdO1xyXG4gIHNwZWNpYWxDaGFyYWN0ZXJzOiBzdHJpbmdbXTtcclxuICBwYXR0ZXJuczoge1xyXG4gICAgW2NoYXJhY3Rlcjogc3RyaW5nXToge1xyXG4gICAgICBwYXR0ZXJuOiBSZWdFeHA7XHJcbiAgICAgIG9wdGlvbmFsPzogYm9vbGVhbjtcclxuICAgIH07XHJcbiAgfTtcclxufVxyXG5cclxuZXhwb3J0IHR5cGUgb3B0aW9uc0NvbmZpZyA9IFBhcnRpYWw8SUNvbmZpZz47XHJcbmV4cG9ydCBjb25zdCBjb25maWc6IEluamVjdGlvblRva2VuPHN0cmluZz4gPSBuZXcgSW5qZWN0aW9uVG9rZW4oJ2NvbmZpZycpO1xyXG5leHBvcnQgY29uc3QgTkVXX0NPTkZJRzogSW5qZWN0aW9uVG9rZW48c3RyaW5nPiA9IG5ldyBJbmplY3Rpb25Ub2tlbihcclxuICAnTkVXX0NPTkZJRydcclxuKTtcclxuZXhwb3J0IGNvbnN0IElOSVRJQUxfQ09ORklHOiBJbmplY3Rpb25Ub2tlbjxJQ29uZmlnPiA9IG5ldyBJbmplY3Rpb25Ub2tlbihcclxuICAnSU5JVElBTF9DT05GSUcnXHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgaW5pdGlhbENvbmZpZzogSUNvbmZpZyA9IHtcclxuICBzdWZpeDogJycsXHJcbiAgcHJlZml4OiAnJyxcclxuICBjbGVhcklmTm90TWF0Y2g6IGZhbHNlLFxyXG4gIHNob3dUZW1wbGF0ZTogZmFsc2UsXHJcbiAgc2hvd01hc2tUeXBlZDogZmFsc2UsXHJcbiAgZHJvcFNwZWNpYWxDaGFyYWN0ZXJzOiB0cnVlLFxyXG4gIHNwZWNpYWxDaGFyYWN0ZXJzOiBbXHJcbiAgICAnLycsXHJcbiAgICAnKCcsXHJcbiAgICAnKScsXHJcbiAgICAnLicsXHJcbiAgICAnOicsXHJcbiAgICAnLScsXHJcbiAgICAnICcsXHJcbiAgICAnKycsXHJcbiAgICAnLCcsXHJcbiAgICAnQCcsXHJcbiAgICAnWycsXHJcbiAgICAnXScsXHJcbiAgICAnXCInLFxyXG4gICAgXCInXCJcclxuICBdLFxyXG4gIHBhdHRlcm5zOiB7XHJcbiAgICAnMCc6IHtcclxuICAgICAgcGF0dGVybjogbmV3IFJlZ0V4cCgnXFxcXGQnKVxyXG4gICAgfSxcclxuICAgICc5Jzoge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdcXFxcZCcpLFxyXG4gICAgICBvcHRpb25hbDogdHJ1ZVxyXG4gICAgfSxcclxuICAgIEE6IHtcclxuICAgICAgcGF0dGVybjogbmV3IFJlZ0V4cCgnW2EtekEtWjAtOV0nKVxyXG4gICAgfSxcclxuICAgIFM6IHtcclxuICAgICAgcGF0dGVybjogbmV3IFJlZ0V4cCgnW2EtekEtWl0nKVxyXG4gICAgfSxcclxuICAgIGQ6IHtcclxuICAgICAgcGF0dGVybjogbmV3IFJlZ0V4cCgnXFxcXGQnKVxyXG4gICAgfSxcclxuICAgIG06IHtcclxuICAgICAgcGF0dGVybjogbmV3IFJlZ0V4cCgnXFxcXGQnKVxyXG4gICAgfVxyXG4gIH1cclxufTtcclxuIl19