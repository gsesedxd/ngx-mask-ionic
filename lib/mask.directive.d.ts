import { NgControl } from '@angular/forms';
import { MaskService } from './mask.service';
import { IConfig } from './config';
export declare class MaskDirective {
    private document;
    private _maskService;
    private _ngControl;
    private _maskValue;
    private _inputValue;
    private _position;
    private _start;
    private _end;
    onTouch: () => void;
    constructor(document: any, _maskService: MaskService, _ngControl: NgControl);
    maskExpression: string;
    specialCharacters: IConfig['specialCharacters'];
    patterns: IConfig['patterns'];
    prefix: IConfig['prefix'];
    sufix: IConfig['sufix'];
    dropSpecialCharacters: IConfig['dropSpecialCharacters'];
    showMaskTyped: IConfig['showMaskTyped'];
    showTemplate: IConfig['showTemplate'];
    clearIfNotMatch: IConfig['clearIfNotMatch'];
    onInput(e: KeyboardEvent): void;
    onBlur(): void;
    onFocus(e: MouseEvent | KeyboardEvent): void;
    onKeyDown(e: KeyboardEvent): void;
    onPaste(): void;
    /** It disables the input element */
    setDisabledState(isDisabled: boolean): void;
    private _repeatPatternSymbols;
    private _initializeMask;
}
