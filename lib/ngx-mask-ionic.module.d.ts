import { optionsConfig } from './config';
import { ModuleWithProviders } from '@angular/core';
export declare class NgxMaskIonicModule {
    static forRoot(configValue?: optionsConfig): ModuleWithProviders<NgxMaskIonicModule>;
    static forChild(configValue?: optionsConfig): ModuleWithProviders<NgxMaskIonicModule>;
}
/**
 * @internal
 */
export declare function _configFactory(initConfig: optionsConfig, configValue: optionsConfig | (() => optionsConfig)): Function | optionsConfig;
